"use strict";

// #############
// Logs cards' uid
// #############

const { NFC } = require('nfc-pcsc');
const nfc = new NFC();
var request = require('request')


nfc.on('reader', reader => {
	console.log(reader.name + ' reader attached, waiting for cards ...');

	reader.on('card', card => {
		console.log(card.uid);
		var options = {
			uri: `http://localhost:3000/api/students/changeAttendance/${card.uid}`,
			method: 'PUT',
			json: {
			  "attendance": "Present"
			}
		  };
		console.log(options.uri)
		request(options, function (error, response, body) {
		if (!error && response.statusCode == 201) {
			console.log("Student has been marked " + options.json.attendance)
		}
		});
	});

	reader.on('error', err => {
		console.error('reader error', err);
	});

	reader.on('end', () => {
		console.log(reader.name + ' reader disconnected.');
	});


});

nfc.on('error', err => {
	console.error(err);
});