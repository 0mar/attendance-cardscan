# NFH Attendance Card Scanning

This is the Nora Frances Henderson Card Scanning project. There is only one file (card.js) that you have to run. This requires one library which made this project VERY easy, it is called  [**nfc-pcsc**](https://github.com/pokusew/nfc-pcsc).


To run this project, simply download it or clone it:

Git:
```bash
git clone https://gitlab.com/0mar/attendance-cardscan.git
```

After downloading/cloning the project, go into the folder. Make sure you have the latest version of Node and NPM installed.
Before running the card.js file, you must install the [**nfc-pcsc**](https://github.com/pokusew/nfc-pcsc) module. 
Simply do this by the following: 

```bash
npm install
```

Once all the necessary modules/dependencies have been installed, run the card.js file using the command:

```bash
node card.js
```

Make sure you have the ACR122U card reader plugged in so that this can work!